#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import urllib2

class ticker:
	url = "https://localbitcoins.com/bitcoinaverage/ticker-all-currencies/"
	data_json = None
	
	#Prices
	avg_1h = 0
	avg_3h = 0
	avg_12h = 0
	avg_24h = 0
	
	def __init__(self, url = url):
		self.refresh()

	def refresh(self):
		self.data_json = json.load(urllib2.urlopen(self.url))
		# self.avg_1h = self.data_json['GBP']['avg_1h'] # depreciated
		# self.avg_3h = self.data_json['GBP']['avg_3h'] # depreciated
		self.avg_12h = self.data_json['GBP']['avg_12h']
		self.avg_24h = self.data_json['GBP']['avg_24h']

	def print_all(self):
		print(json.dumps(self.data_json, indent=2))
		
	def print_GBP(self):	
		print(u"Avg  1h: £" + str("%.2f" % self.avg_1h) + " /BTC")
		print(u"Avg  3h: £" + str("%.2f" % self.avg_3h) + " /BTC")
		print(u"Avg 12h: £" + str("%.2f" % self.avg_12h) + " /BTC")
		print(u"Avg 24h: £" + str("%.2f" % self.avg_24h) + " /BTC")
		
		
#Main program
def main():
	lb = ticker()
	lb.print_GBP()
	#print(lb.avg_1h)
    
if __name__ == "__main__":
    main()
